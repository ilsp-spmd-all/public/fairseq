from fairseq.data.encoders import characters

# Generate GENERATION_DISTRIBUTION_SIZE errors per word and keep most common
# SAMPLE_MISSPELLINGS_PER_WORD errors
SAMPLE_MISSPELLINGS_PER_WORD = 20
GENERATION_DISTRIBUTION_SIZE = 10000


ACCENT = chr(769)

CHARACTER_VOCAB = [
    " ",
    ACCENT,
    characters.SPACE_ESCAPE,
    "!",
    '"',
    "%",
    "&",
    "'",
    "(",
    ")",
    "*",
    "+",
    ",",
    "-",
    ".",
    "/",
    ":",
    ";",
    "=",
    "[",
    "]",
    "_",
    "«",
    "»",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "α",
    "β",
    "γ",
    "δ",
    "ε",
    "ζ",
    "η",
    "θ",
    "ι",
    "κ",
    "λ",
    "μ",
    "ν",
    "ξ",
    "ο",
    "π",
    "ρ",
    "ς",
    "σ",
    "τ",
    "υ",
    "φ",
    "χ",
    "ψ",
    "ω",
    "Α",
    "Β",
    "Γ",
    "Δ",
    "Ε",
    "Ζ",
    "Η",
    "Θ",
    "Ι",
    "Κ",
    "Λ",
    "Μ",
    "Ν",
    "Ξ",
    "Ο",
    "Π",
    "Ρ",
    "Σ",
    "Τ",
    "Υ",
    "Φ",
    "Χ",
    "Ψ",
    "Ω",
]


CHARACTERS = [
    "α",
    "β",
    "γ",
    "δ",
    "ε",
    "ζ",
    "η",
    "θ",
    "ι",
    "κ",
    "λ",
    "μ",
    "ν",
    "ξ",
    "ο",
    "π",
    "ρ",
    "σ",
    "τ",
    "υ",
    "φ",
    "χ",
    "ψ",
    "ω",
    "ς",
]

VOWELS = ["α", "ε", "η", "ι", "ο", "υ", "ω"]

BIGRAMS = CHARACTERS + list(set([c1 + c2 for c1 in CHARACTERS for c2 in CHARACTERS]))


NUMBERS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]


UNIGRAM_MISSPELLINGS = [
    ("ο", "ω"),
    ("ω", "ο"),
    ("υ", "ι"),
    ("ι", "υ"),
    ("η", "ι"),
    ("ι", "η"),
    ("υ", "η"),
    ("η", "υ"),
    ("ς", "σ"),
    # ("σ", "ς"),
    ("0", "ο"),
    ("ο", "0"),
    ("3", "ξ"),
    ("ξ", "3"),
    ("8", "θ"),
    ("θ", "8"),
    ("9", "θ"),
    ("θ", "9"),
]


KEYBOARD_NEIGHBORS = [
    ("ε", "ς"),
    ("ς", "ε"),
    ("ε", "ρ"),
    ("ρ", "ε"),
    ("ρ", "τ"),
    ("τ", "ρ"),
    ("τ", "υ"),
    ("υ", "τ"),
    ("υ", "θ"),
    ("θ", "υ"),
    ("θ", "ι"),
    ("ι", "θ"),
    ("ι", "ο"),
    ("ο", "ι"),
    ("ο", "π"),
    ("π", "ο"),
    ("α", "σ"),
    ("σ", "α"),
    ("σ", "δ"),
    ("δ", "σ"),
    ("δ", "φ"),
    ("φ", "δ"),
    ("φ", "γ"),
    ("γ", "φ"),
    ("γ", "η"),
    ("η", "γ"),
    ("η", "ξ"),
    ("ξ", "η"),
    ("ξ", "κ"),
    ("κ", "ξ"),
    ("κ", "λ"),
    ("λ", "κ"),
    ("ζ", "χ"),
    ("χ", "ζ"),
    ("χ", "ψ"),
    ("ψ", "χ"),
    ("ω", "ψ"),
    ("ψ", "ω"),
    ("ω", "β"),
    ("β", "ω"),
    ("ν", "β"),
    ("β", "ν"),
    ("μ", "ν"),
    ("ν", "μ"),
]


BIGRAM_MISSPELLINGS = [
    ("αι", "ε"),
    ("ε", "αι"),
    ("ει", "υ"),
    ("υ", "ει"),
    ("ει", "ι"),
    ("ι", "ει"),
    ("ει", "η"),
    ("η", "ει"),
    ("ει", "οι"),
    ("οι", "ει"),
    ("οι", "ι"),
    ("ι", "οι"),
    ("οι", "η"),
    ("η", "οι"),
    ("οι", "υ"),
    ("υ", "οι"),
    ("σσ", "σ"),
    ("σ", "σσ"),
    ("λλ", "λ"),
    ("λ", "λλ"),
    ("κκ", "κ"),
    ("κ", "κκ"),
    ("ρρ", "ρ"),
    ("ρ", "ρρ"),
    ("ττ", "τ"),
    ("τ", "ττ"),
    ("ππ", "π"),
    ("π", "ππ"),
    ("αα", "α"),
    ("α", "αα"),
    ("γγ", "γκ"),
    ("γκ", "γγ"),
    ("γγ", "γ"),
    ("γ", "γγ"),
    ("γχ", "χ"),
    ("χ", "γχ"),
    ("μπ", "π"),
    ("π", "μπ"),
    ("μμ", "μ"),
    ("μ", "μμ"),
    ("νν", "ν"),
    ("ν", "νν"),
]

UNIGRAM_SPELLING_ERRORS = UNIGRAM_MISSPELLINGS

UNIGRAM_ERRORS = UNIGRAM_MISSPELLINGS + KEYBOARD_NEIGHBORS
BIGRAM_ERRORS = BIGRAM_MISSPELLINGS
