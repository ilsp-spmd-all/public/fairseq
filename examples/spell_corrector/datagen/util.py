import pickle

from joblib import Parallel
from tqdm.auto import tqdm


class ParallelRunner(Parallel):
    def __init__(self, use_tqdm=True, total=None, *args, **kwargs):
        self._use_tqdm = use_tqdm
        self._total = total
        super().__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        with tqdm(disable=not self._use_tqdm, total=self._total) as self._pbar:
            return Parallel.__call__(self, *args, **kwargs)

    def print_progress(self):
        if self._total is None:
            self._pbar.total = self.n_dispatched_tasks
        self._pbar.n = self.n_completed_tasks
        self._pbar.refresh()


def pickle_dump(data, fname):
    with open(fname, "wb") as fd:
        pickle.dump(data, fd)


def find_substring_occurences(string, substring):
    """
    Function: Returning all the index of substring in a string
    Arguments: String and the search string
    Return:Returning a list
    """
    length = len(substring)
    c = 0
    indexes = []

    while c < len(string):
        if string[c : c + length] == substring:
            indexes.append(c)
        c = c + 1

    return indexes
