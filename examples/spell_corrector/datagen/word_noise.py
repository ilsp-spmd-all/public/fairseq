import argparse
import random
import unicodedata
from collections import Counter

import numpy as np
from joblib import delayed

from examples.spell_corrector.datagen import constants
from examples.spell_corrector.datagen.util import (ParallelRunner,
                                                   find_substring_occurences)


def filter_counts(counts, thres=10):
    filt = {}

    for k, v in counts.items():
        if v > thres:
            filt[k] = v

    return filt


def ignore_token(w):
    # Ignore word that doesn't contain a greek character
    dont_ignore = any(ch in constants.CHARACTERS for ch in w) and len(w) > 2

    return not dont_ignore


def insert_error_to_word(word, use_common_errors=True, bigram=False, error_type="ins"):
    if ignore_token(word):
        return word

    allowed = constants.CHARACTERS if not bigram else constants.BIGRAMS
    error = None

    def random_error(errors):
        valid_errors = []

        for c1, c2 in errors:
            if c1 in word:
                valid_errors.append((c1, c2))
        error = random.choice(valid_errors) if len(valid_errors) > 0 else None

        return error

    if error_type == "del":
        bigram = False
        idx = random.randint(0, len(word) - 1)
        to_insert = ""
        continue_idx = idx + 1
    elif error_type == "ins":
        bigram = False

        if use_common_errors:
            error = random_error(constants.KEYBOARD_NEIGHBORS)

        if not use_common_errors or error is None:
            idx = random.randint(0, len(word) - 1)
            to_insert = allowed[np.random.randint(0, len(allowed))]
        else:
            idx = np.random.choice(find_substring_occurences(word, error[0]))
            to_insert = error[1]
        continue_idx = idx
    elif error_type == "sub":
        if use_common_errors:
            # errors = constants.UNIGRAM_ERRORS if not bigram else constants.BIGRAM_ERRORS
            errors = (
                constants.UNIGRAM_SPELLING_ERRORS

                if not bigram
                else constants.BIGRAM_ERRORS
            )
            error = random_error(errors)

        if not use_common_errors or error is None:
            idx = random.randint(0, len(word) - 1)
            to_insert = allowed[np.random.randint(0, len(allowed))]
            continue_idx = idx + 1 if not bigram else idx + random.randint(1, 2)
        else:
            idx = np.random.choice(find_substring_occurences(word, error[0]))
            to_insert = error[1]
            continue_idx = idx + len(error[0])
    elif error_type == "shuf":
        idx = random.randint(0, len(word) - 2)
        to_insert = word[idx + 1] + word[idx]
        continue_idx = idx + 2
    else:
        raise ValueError("Unknown error type. Select: [ins|del|sub|shuf]")

    return word[:idx] + to_insert + word[continue_idx:]


def word_noise(word, num_errors=1, use_common_errors=True, mistypes=False):
    num_errors = min(num_errors, max(len(word) - 4, 1))

    MISTYPES = ["shuf", "ins", "del"]

    for _ in range(num_errors):

        if mistypes:
            error_type = np.random.choice(MISTYPES)
        else:
            error_type = "sub"
        bigram = np.random.choice([True, False], p=[0.3, 0.7])

        word = insert_error_to_word(
            word,
            use_common_errors=use_common_errors,
            bigram=bigram,
            error_type=error_type,
        )

    return word


def create_word_misspellings_distribution(
    word, iterations=100, use_common_errors=True, mistypes=False
):
    misspellings = []

    for _ in range(iterations):
        num_errors = np.random.choice([1, 2], p=[0.8, 0.2])
        noisy_word = word_noise(
            word,
            num_errors=num_errors,
            use_common_errors=use_common_errors,
            mistypes=mistypes,
        )

        if noisy_word != word:
            misspellings.append((noisy_word, word))

    counts = Counter(misspellings)

    return counts


def create_word_misspellings(
    word, iterations=100, use_common_errors=True, mistypes=False
):
    counts = create_word_misspellings_distribution(
        word,
        iterations=iterations,
        use_common_errors=use_common_errors,
        mistypes=mistypes,
    )
    misspellings = dict(
        counts.most_common(min(constants.SAMPLE_MISSPELLINGS_PER_WORD, len(counts)))
    ).keys()

    return list(misspellings)


def mkspellcorpus(words, n_jobs=32, use_common_errors=False, mistypes=False):
    corpus = ParallelRunner(n_jobs=n_jobs, total=len(words))(
        delayed(create_word_misspellings)(
            word,
            iterations=constants.GENERATION_DISTRIBUTION_SIZE,
            use_common_errors=use_common_errors,
            mistypes=mistypes,
        )

        for word in words
    )
    corpus = [el for sublist in corpus for el in sublist]

    return corpus


def parse_args():
    parser = argparse.ArgumentParser("Generate word misspelling corpus")
    parser.add_argument("--vocab", type=str, help="Vocabulary file")
    parser.add_argument("--njobs", type=int, help="njobs")
    parser.add_argument("--common", action="store_true", help="Use common errors only")
    parser.add_argument(
        "--mistypes",
        action="store_true",
        help="Generate mistyping errors if true else misspellings",
    )
    parser.add_argument("--output", type=str, help="Output file")
    args = parser.parse_args()

    return args


def main():
    args = parse_args()
    vocab = args.vocab
    corpus_file = args.output
    use_common_errors = args.common
    mistypes = args.mistypes
    with open(vocab, "r") as fd:
        words = [unicodedata.normalize("NFD", l.strip()) for l in fd]

    corpus = mkspellcorpus(
        words,
        n_jobs=args.njobs,
        use_common_errors=use_common_errors,
        mistypes=mistypes,
    )
    with open(corpus_file, "w") as fd:
        for src, tgt in corpus:
            fd.write("{}\t{}\n".format(src, tgt))


if __name__ == "__main__":
    main()
