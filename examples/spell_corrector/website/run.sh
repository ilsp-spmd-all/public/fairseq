
SCRIPT_PATH="$(dirname $(readlink -f $0))"

USER_DIR=/fairseq_user_dir/examples/spell_corrector

SERVICE_HOST=0.0.0.0
SERVICE_PORT=${SERVICE_PORT:-5000}
SPELL_CONFIG=${SPELL_CONFIG:-spell-gr-small}
CKPT_FILE=/checkpoints/${SPELL_CONFIG}.pt
echo "Running on http://$SERVICE_HOST:$SERVICE_PORT"

python $USER_DIR/website/predict.py \
    --task spell-corrector \
    --webhost $SERVICE_HOST \
    --webport $SERVICE_PORT \
    --path $CKPT_FILE \
    --user-dir $USER_DIR
