import argparse
import unicodedata
from functools import lru_cache

from flask import Flask, jsonify, render_template, request, url_for
from flask_cors import CORS, cross_origin


from examples.spell_corrector.predictor import make_corrector_for_website

spell_correct, SERVICE_HOST, PORT = make_corrector_for_website()

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route("/", methods=["GET"])
def index():
    return render_template(
        "index.html", service_url="http://{}:{}/correct".format(SERVICE_HOST, PORT)
    )


@lru_cache(maxsize=65536)
def chunk_spell_correct(text):
    max_len = 80
    text = unicodedata.normalize("NFD", text)
    # FIXME: Weird issue when sentence ends with semicolon. Need to investigate
    is_question = False
    if text.endswith(";"):
        text = text[:-1]
        is_question = True

    words = text.split(" ")
    # Fix me. Use sentence tokenize to split into sentences first. This can mess the context
    chunks = []
    current_chunk = ""

    for w in words:
        if len(current_chunk) + len(w) + 1 < max_len:
            current_chunk = current_chunk + " " + w
        else:
            chunks.append(current_chunk)
            current_chunk = ""

    if len(chunks) == 0 or current_chunk != chunks[-1]:
        chunks.append(current_chunk)

    transformed_text = []

    for chunk in chunks:
        if len(chunk) > 0:
            corrected = spell_correct([chunk])
            if len(transformed_text) == 0:
                transformed_text = ["" for _ in range(len(corrected))]

            for i, correct_chunk in enumerate(corrected):
                transformed_text[i] = (
                    transformed_text[i]
                    + " "
                    + unicodedata.normalize("NFC", correct_chunk)
                )
    transformed_text = [t.lstrip() for t in transformed_text]
    if is_question:
        transformed_text = [t + ";" for t in transformed_text]
    return transformed_text


@app.route("/correct", methods=["POST"])
def correct():
    sentence = request.json["sentence"]
    sentence = unicodedata.normalize("NFD", sentence)
    corrected = chunk_spell_correct(sentence)  # spell_correct([sentence])

    return jsonify({"correct": corrected})


if __name__ == "__main__":
    from waitress import serve

    serve(app, host="0.0.0.0", port=PORT)
