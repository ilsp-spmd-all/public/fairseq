# i!/usr/bin/env python3 -u
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
"""
Translate raw text with a trained model. Batches data on-the-fly.
"""

import logging
import math
import os
import sys
import time
from collections import namedtuple

import numpy as np
import torch

from fairseq import checkpoint_utils, options, tasks, utils
from fairseq.data import encoders


def get_symbols_to_strip_from_output(generator):
    if hasattr(generator, "symbols_to_strip_from_output"):
        return generator.symbols_to_strip_from_output
    else:
        return {generator.eos}


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
fh = logging.FileHandler("/tmp/misspellings.log")
fh.setLevel(logging.INFO)
logger.addHandler(fh)


Batch = namedtuple("Batch", "ids src_tokens src_lengths constraints")


def make_batches(lines, args, task, max_positions, encode_fn):
    def encode_fn_target(x):
        return encode_fn(x)

    tokens = [
        task.source_dictionary.encode_line(
            encode_fn(src_str), add_if_not_exist=False
        ).long()

        for src_str in lines
    ]
    lengths = [t.numel() for t in tokens]
    itr = task.get_batch_iterator(
        dataset=task.build_dataset_for_inference(tokens, lengths, constraints=None),
        max_tokens=args.max_tokens,
        max_sentences=args.max_sentences,
        max_positions=max_positions,
        ignore_invalid_inputs=args.skip_invalid_size_inputs_valid_test,
    ).next_epoch_itr(shuffle=False)

    for batch in itr:
        ids = batch["id"]
        src_tokens = batch["net_input"]["src_tokens"]
        src_lengths = batch["net_input"]["src_lengths"]
        yield Batch(
            ids=ids,
            src_tokens=src_tokens,
            src_lengths=src_lengths,
            constraints=None,
        )


def make_corrector_fn(args):
    utils.import_user_module(args)

    if args.max_tokens is None and args.max_sentences is None:
        args.max_sentences = 1

    assert (
        not args.sampling or args.nbest == args.beam
    ), "--sampling requires --nbest to be equal to --beam"

    print(args)

    # Fix seed for stochastic decoding

    if args.seed is not None and not args.no_seed_provided:
        np.random.seed(args.seed)
        utils.set_torch_seed(args.seed)

    use_cuda = torch.cuda.is_available() and not args.cpu

    # Setup task, e.g., translation
    task = tasks.setup_task(args)

    # Load ensemble
    print("loading model(s) from {}".format(args.path))
    models, _model_args = checkpoint_utils.load_model_ensemble(
        args.path.split(os.pathsep),
        arg_overrides=eval(args.model_overrides),
        task=task,
        suffix=getattr(args, "checkpoint_suffix", ""),
    )

    # Set dictionaries
    src_dict = task.source_dictionary
    tgt_dict = task.target_dictionary

    # Optimize ensemble for generation

    for model in models:
        model.prepare_for_inference_(args)

        if args.fp16:
            model.half()

        if use_cuda:
            model.cuda()

    # Initialize generator
    generator = task.build_generator(models, args)

    # Handle tokenization and BPE
    tokenizer = encoders.build_tokenizer(args)
    bpe = encoders.build_bpe(args)

    def encode_fn(x):
        if tokenizer is not None:
            x = tokenizer.encode(x)

        if bpe is not None:
            x = bpe.encode(x)

        return x

    def decode_fn(x):
        if bpe is not None:
            x = bpe.decode(x)

        if tokenizer is not None:
            x = tokenizer.decode(x)

        return x

    # Load alignment dictionary for unknown word replacement
    # (None if no unknown word replacement, empty if no path to align dictionary)
    align_dict = utils.load_align_dict(args.replace_unk)

    max_positions = utils.resolve_max_positions(
        task.max_positions(), *[model.max_positions() for model in models]
    )

    print("NOTE: hypothesis and token scores are output in base 2")

    def spellcorrect(inputs):
        results = []
        total_translate_time = 0
        processed = 0
        print("USE CUDA {}".format(use_cuda))
        batches = make_batches(inputs, args, task, max_positions, encode_fn)

        for batch in batches:
            src_tokens = batch.src_tokens
            src_lengths = batch.src_lengths

            if use_cuda:
                src_tokens = src_tokens.cuda()
                src_lengths = src_lengths.cuda()

            sample = {
                "net_input": {
                    "src_tokens": src_tokens,
                    "src_lengths": src_lengths,
                },
            }
            translate_start_time = time.time()
            translations = task.inference_step(
                generator, models, sample, constraints=None
            )
            translate_time = time.time() - translate_start_time
            total_translate_time += translate_time

            for i, (id, hypos) in enumerate(zip(batch.ids.tolist(), translations)):
                src_tokens_i = utils.strip_pad(src_tokens[i], tgt_dict.pad())
                src_str = decode_fn(src_dict.string(src_tokens_i, args.remove_bpe))
                curr_hypos = []

                for hypo in hypos[: args.nbest]:
                    hypo_tokens, hypo_str, alignment = utils.post_process_prediction(
                        hypo_tokens=hypo["tokens"].int().cpu(),
                        src_str=src_str,
                        alignment=hypo["alignment"],
                        align_dict=align_dict,
                        tgt_dict=tgt_dict,
                        remove_bpe=args.remove_bpe,
                        extra_symbols_to_ignore=get_symbols_to_strip_from_output(
                            generator
                        ),
                    )
                    detok_hypo_str = decode_fn(hypo_str)
                    curr_hypos.append(detok_hypo_str)
                logger.info("Input: {}\tCorrection: {}".format(src_str, curr_hypos[0]))
                print("Input: {}\tCorrection: {}".format(src_str, curr_hypos[0]))
                results.append((id, curr_hypos))
            processed += batch.ids.shape[0]

            if args.log_progress:
                print(
                    "Processed: {} / {}, lengths = {:.2f}".format(
                        processed,
                        len(inputs),
                        src_lengths.sum().item() / src_lengths.numel(),
                    )
                )

        print("Total time: {}".format(total_translate_time))
        results = [x[1] for x in sorted(results, key=lambda x: x[0])]

        if args.nbest == 1:
            # flatten
            results = [x[0] for x in results]

        if len(inputs) == 1:
            results = results[0]

        return results

    print("Model Loaded...")

    return spellcorrect


def make_corrector_for_website():
    parser = options.get_interactive_generation_parser()
    parser.add_argument("--webhost", type=str, default="0.0.0.0", help="Hostname")
    parser.add_argument("--webport", type=int, default=5000, help="Port")

    args = options.parse_args_and_arch(parser)
    args.log_progress = False
    spell_corrector = make_corrector_fn(args)

    return spell_corrector, args.webhost, args.webport
