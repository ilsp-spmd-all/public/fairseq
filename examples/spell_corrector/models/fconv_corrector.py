from fairseq.models import register_model, register_model_architecture
from fairseq.models.fconv import FConvModel, base_architecture


@register_model("fconv-modified")
class FConvModified(FConvModel):
    @staticmethod
    def add_args(parser):
        super(FConvModified, FConvModified).add_args(parser)
        parser.add_argument(
            "--max-source-positions", type=int, metavar="N", help="Max source length"
        )
        parser.add_argument(
            "--max-target-positions", type=int, metavar="N", help="Max target length"
        )


@register_model_architecture("fconv-modified", "fconv_spell_large")
def fconv_spell_large(args):
    convs = "[(512, 3)] * 9"  # first 9 layers have 512 units
    convs += " + [(1024, 3)] * 4"  # next 4 layers have 1024 units
    convs += " + [(2048, 1)] * 2"  # final 2 layers use 1x1 convolutions

    args.max_source_positions = getattr(args, "max_source_positions", 200)
    args.max_target_positions = getattr(args, "max_target_positions", 200)
    args.decoder_attention = getattr(args, "decoder_attention", "True")
    args.dropout = getattr(args, "dropout", 0.3)

    args.encoder_embed_dim = getattr(args, "encoder_embed_dim", 512)
    args.decoder_embed_dim = getattr(args, "decoder_embed_dim", 512)
    args.decoder_out_embed_dim = getattr(args, "decoder_out_embed_dim", 512)
    args.share_input_output_embed = getattr(args, "share_input_output_embed", True)

    args.encoder_layers = getattr(args, "encoder_layers", convs)
    args.decoder_layers = getattr(args, "decoder_layers", convs)

    base_architecture(args)


@register_model_architecture("fconv-modified", "fconv_spell_basic")
def fconv_spell_basic(args):
    convs = "[(512, 5)] * 10"

    args.max_source_positions = getattr(args, "max_source_positions", 200)
    args.max_target_positions = getattr(args, "max_target_positions", 200)
    args.decoder_attention = getattr(args, "decoder_attention", "True")
    args.dropout = getattr(args, "dropout", 0.3)

    args.encoder_embed_dim = getattr(args, "encoder_embed_dim", 256)
    args.decoder_embed_dim = getattr(args, "decoder_embed_dim", 256)
    args.decoder_out_embed_dim = getattr(args, "decoder_out_embed_dim", 256)
    args.share_input_output_embed = getattr(args, "share_input_output_embed", True)

    args.encoder_layers = getattr(args, "encoder_layers", convs)
    args.decoder_layers = getattr(args, "decoder_layers", convs)

    base_architecture(args)
