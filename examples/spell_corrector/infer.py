import unicodedata

from examples.spell_corrector.predictor import make_corrector_fn
from examples.spell_corrector.wer_utils import calc_wer_stats
from fairseq import options


def read_file(fname):
    with open(fname, "r") as fd:
        lines = [unicodedata.normalize("NFD", ln.strip()) for ln in fd]

    return lines


def add_args(parser):
    group = parser.add_argument_group("Interactive")
    group.add_argument(
        "--misspelled-file",
        type=str,
        metavar="FILE",
        help="File containing misspelled sentences",
    )
    group.add_argument(
        "--ground-truth-file",
        type=str,
        metavar="FILE",
        help="File containing ground truth sentences",
    )
    group.add_argument(
        "--log-progress",
        default=False,
        action="store_true",
        help="Use logging",
    )
    group.add_argument(
        "--log-predictions",
        default=False,
        action="store_true",
        help="Use logging",
    )


def make_corrector():
    parser = options.get_interactive_generation_parser()
    add_args(parser)
    args = options.parse_args_and_arch(parser)
    spell_corrector = make_corrector_fn(args)

    return spell_corrector


def evaluate():
    parser = options.get_interactive_generation_parser()
    add_args(parser)
    args = options.parse_args_and_arch(parser)

    spell_corrector = make_corrector_fn(args)
    misspelled = read_file(args.misspelled_file)
    ground_truths = read_file(args.ground_truth_file)

    corrected = spell_corrector(misspelled)

    errors, num_words = 0, 0

    for hyp, ref in zip(corrected, ground_truths):
        stats = calc_wer_stats(hyp, ref)
        curr_errors = stats["ins"] + stats["dels"] + stats["subs"]
        errors += curr_errors
        num_words += stats["numwords"]

    wer = 100.0 * errors / num_words
    print("WER: {}".format(wer))

    return wer


if __name__ == "__main__":
    evaluate()
