import logging
import os
import random
import sys
import unicodedata

import numpy as np
import torch
from tqdm import tqdm

from examples.spell_corrector.datagen.constants import CHARACTER_VOCAB
from examples.spell_corrector.datagen.sentence_noise import (
    make_sentence_noise_fn, read_word_misspellings)
from fairseq.data import Dictionary, LanguagePairDataset
from fairseq.data.encoders import characters, sentencepiece_bpe
from fairseq.tasks import FairseqTask, register_task

logging.basicConfig(
    format="%(asctime)s | %(levelname)s | %(name)s | %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=os.environ.get("LOGLEVEL", "INFO").upper(),
    stream=sys.stdout,
)

logger = logging.getLogger("fairseq.examples.spell_corrector")


def mkdict(characters):
    voc = Dictionary()

    for c in list(set(characters)):
        voc.add_symbol(c)
    voc.finalize()

    return voc


@register_task("spell-corrector")
class SpellCorrectionTask(FairseqTask):
    @staticmethod
    def add_args(parser):
        parser.add_argument("--train", type=str, help="Train split file")
        parser.add_argument("--val", type=str, help="Validation split file")
        parser.add_argument("--test", type=str, help="Test split file")
        parser.add_argument(
            "--word-errors", type=str, default=None, help="Word error corpus"
        )
        parser.add_argument(
            "--spell-tokenizer", type=str, help="Tokenizer for the spell corpus"
        )

    @classmethod
    def default_config(cls, args):
        # WE MUST specify --max-sentences and --save-dir from cli
        args.log_format = "tqdm"

        # larger number breaks multi-gpu training.
        # https://github.com/pytorch/fairseq/issues/1057
        # Other solutions did not work
        args.num_workers = 0

        # Currently only works at character level.
        # In the future we may extend to wordpieces.
        args.spell_tokenizer = "characters"

        # For website
        args.bpe = "characters"
        args.beam = 5
        args.nbest = 5

        # We don't handle larger sentences. Skip them.
        args.skip_invalid_size_inputs_valid_set = True

        # Optimizer configuration
        args.optimizer = "adam"
        args.adam_betas = "(0.9, 0.98)"
        args.weight_decay = 0.01
        args.lr = 0.001

        # LR scheduler configuration
        args.lr_scheduler = "inverse_sqrt"
        args.warmup_init_lr = 1e-07
        args.warmup_updates = 8000

        # Training configuration
        args.patience = 3
        args.max_update = 400000
        args.clip_norm = 0.1
        args.save_interval_updates = 5000
        args.keep_interval_updates = 100
        args.sentence_avg = True
        args.required_batch_size_multiple = 2

        logger.info("Overriding with default args from spell_corrector task")
        logger.info(args)

        return args

    @classmethod
    def setup_task(cls, args, **kwargs):
        args = cls.default_config(args)

        char_vocab = mkdict(CHARACTER_VOCAB)
        word_misspellings = read_word_misspellings(args.word_errors)
        sentence_noiser = make_sentence_noise_fn(word_misspellings=word_misspellings)

        if args.spell_tokenizer == "characters":
            tokenizer = characters.Characters(args)
        elif args.spell_tokenizer == "wordpiece":
            tokenizer = sentencepiece_bpe.SentencepieceBPE(args)
        else:
            raise ValueError("Unsupported spell tokenizer")

        return SpellCorrectionTask(args, char_vocab, sentence_noiser, tokenizer)

    def __init__(self, args, char_vocab, sentence_noiser, tokenizer):
        super().__init__(args)
        self.char_vocab = char_vocab
        self.sentence_noiser = sentence_noiser
        self.tokenizer = tokenizer

    def load_dataset(self, split, **kwargs):
        if split == "train":
            fname = self.args.train
        elif split == "valid":
            fname = self.args.val
        elif split == "test":
            fname = self.args.test
        else:
            raise ValueError("Invalid split {}".format(split))

        source, target, lengths_source, lengths_target = [], [], [], []
        with open(fname, "r") as fd:
            for ln in tqdm(fd.readlines()):
                sentence = ln.strip()
                sentence = unicodedata.normalize("NFD", sentence)
                misspelled = self.sentence_noiser(sentence)
                target_tokens = self.char_vocab.encode_line(
                    self.tokenizer.encode(sentence),
                    add_if_not_exist=False,
                    # line_tokenizer=tokenize_line,
                ).long()
                source_tokens = self.char_vocab.encode_line(
                    self.tokenizer.encode(misspelled),
                    add_if_not_exist=False,
                    # line_tokenizer=tokenize_line,
                ).long()
                source.append(source_tokens)
                target.append(target_tokens)
                lengths_source.append(source_tokens.numel())
                lengths_target.append(target_tokens.numel())

        assert len(source) == len(target)

        logger.info("| {} {} examples".format(split, len(source)))

        self.datasets[split] = LanguagePairDataset(
            src=source,
            src_sizes=lengths_source,
            src_dict=self.char_vocab,
            tgt=target,
            tgt_sizes=lengths_target,
            tgt_dict=self.char_vocab,
            left_pad_source=False,
            input_feeding=True,  # Teacher forcing
            shuffle=True,
            remove_eos_from_source=True,
            append_eos_to_target=True,
            append_bos=True,
        )

    def max_positions(self):
        return (80, 80)

    def build_dataset_for_inference(self, src_tokens, src_lengths, constraints=None):
        return LanguagePairDataset(
            src=src_tokens,
            src_sizes=src_lengths,
            src_dict=self.char_vocab,
            tgt_dict=self.char_vocab,
            left_pad_source=False,
            input_feeding=True,  # Teacher forcing
            shuffle=False,
            remove_eos_from_source=True,
            append_eos_to_target=True,
            append_bos=True,
        )

    @property
    def source_dictionary(self):
        return self.char_vocab

    @property
    def target_dictionary(self):
        return self.char_vocab
